******************************************
The Android/Morello initial release (2020)
******************************************

A. Introduction
===============

Welcome to the Android/Morello code base.

The project is aiming at introducing everyone to the world of capability-aware
software. The document is a guide on what has been done already, on how to try
Android/Morello, and on potential future directions of the research.

The aim of this release is to provide a minimal basis for the future research.

This release is based on Android Q (10.0.0_r2) and is providing the minimal
system Android profile (:code:`fvp_nano` target) starting on the Morello FVP
(model) - booting the system to console shell. There are provisional plans to
support full Android boot by the next year.

In this release, only a small subset of Android components are ported to
Morello Pure-cap ABI [B]. There are plans to extend the Pure-cap ABI support
further. Please, contribute source code changes and ideas on useful use-cases.

The initially ported Android components include:

- the standard C library (Bionic)
- the logd and servicemanager services

Additionally, this release provides:

- core Morello support in the kernel
- basic demo of capability-based compartmentalization

The Morello architecture does not include AArch32, and therefore no support for
AArch32 is included in this release.

B. Hybrid-cap and Pure-cap ABIs
===============================

Morello provides support of two ABIs: Hybrid-cap and Pure-cap.

**Hybrid-cap** (Hybrid-capability)
 Pointers are addresses (64-bit values, as in the Base - usual AArch64 - ABI)
 unless explicitly declared as capabilities.

**Pure-cap** (Pure-capability)
 Pointers are always 128+1-bit capabilities [F.1].

When this document says that an Android software component was ported to
Morello, that means it was changed (where necessary) to support being compiled
for the Pure-cap ABI. Ports of system libraries are available in both the
Hybrid-cap and the Pure-cap ABIs, while ported services are only built in the
Pure-cap ABIs.

Android software components that haven't been ported to Morello for this release
are being built only in the Hybrid-cap ABI. We choose to use the Pure-cap ABI
for Morello ports since it introduces capability-based features without explicit
variable annotations, which would have been required when porting in Hybrid-cap
ABI. Unless changed to explicitly use capabilities, the software built in the
Hybrid-cap behaves as if it was compiled for the Base ABI. Since effectively the
software components behave the same in the Hybrid-cap and Base ABIs, sometimes
their ABI is referred to as the Base ABI.


C. Morello ports and demonstrations
===================================

1. Bionic
---------

Source: `bionic`_

Porting Bionic is a prerequisite for porting anything else in the Android
user-space, since Bionic is the standard C library and provides application
startup as well as the most common interfaces like memcpy, fopen, sprintf and
many more of those.

Support for the Pure-cap ABI required some changes to `bionic`_ libc. This
section is a high level overview of these changes. For full details please
consult Bionic's git history and the respective code snippets.

An important dependency of Bionic, which also was ported to Pure-cap is the
`jemalloc`_ memory allocator. The allocator interface currently doesn't narrow
bounds in capabilities it returns, however restricting the bounds is planned for
the nearest future as setting tightest bounds is an important step towards
preventing heap memory usage errors.

It is worth noting that at the moment only static linkage is supported, and
there are plans to support dynamic linkage in the near future.

1.1. libshim translation layer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The major change to Bionic is that system calls are no longer generated as
usual for both the Base / Hybrid-cap and Pure-cap ABIs. Instead, there is a
thin system call translation layer (`libshim`_) introduced which enables
operation of the Pure-cap ABI executables while the kernel only supports Base
ABI. This is providing only limited emulation of kernel Pure-cap ABI support
and is a temporary stage only - until the Pure-cap ABI is supported in the
kernel.

Additionally, libshim converts the command line arguments, environment and
auxiliary vectors upon the process startup to comply with the expected memory
layout in the Pure-cap ABI, because the original layout is prepared by the
kernel which currently provides the Base ABI even for Pure-cap user-space.

1.2. Capability reconstruction at process startup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

While in the Base ABI pointers are just addresses, in the Pure-cap ABI the
pointers are required to be capabilities. This includes the pointers in global
data section. In the Base ABI, these values would be hard-coded into the binary.
However capabilities are intrinsically dynamic values which have to be
constructed following architecturally-defined rules in runtime. For the Pure-cap
ABI, the executable is extended with a table of capability descriptors
(__cap_relocs ELF section), which are used during the process initialization to
construct the required capability values. This is done by :code:`__morello_init`
(`morello_init.S`_) which is invoked by :code:`_start` (`crtbegin.c`_). The
__cap_relocs section is only needed in statically linked binaries, since dynamic
relocations should be enough to describe the values to be constructed.

1.3. Extended set of string-processing routines
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Basic string routines - memcpy, mempcpy, memset, memmove, memcmp - take
pointers, which means capabilities in the Pure-cap ABI and addresses in the
Hybrid-cap ABI. In the Hybrid-cap ABI, applications might need to use the
functions on buffers referred to through explicitly declared capabilities. For
this purpose, the standard string routines are complemented with counterparts
accepting capabilities: memcpy_c, mempcpy_c, memset_c, memmove_c, memcmp_c.

For memcpy, mempcpy, memmove both of the versions are implemented in a way that
guarantees the capability tags are preserved when copying memory content.

1.4. Other changes
^^^^^^^^^^^^^^^^^^

Additional changes in Bionic are mostly related to the manipulation of pointer
values. Details on the particular changes are provided in the relevant commit
messages. The common changes required for porting to Morello are summarised in
the section [D] below.

1.5. Unit tests
^^^^^^^^^^^^^^^

Bionic's unit tests (:code:`bionic-unit-tests-static`), which are built both
for the Hybrid-cap and Pure-cap ABIs, are located at the following paths in the
output file system:

.. code-block:: sh

  /data/nativetest64/bionic-unit-tests-static/bionic-unit-tests-static

for the Hybrid-cap ABI, and

.. code-block:: sh

  /data/nativetestc64/bionic-unit-tests-static/bionic-unit-tests-static

for the Pure-cap ABI.

The Bionic test suite is extended to test the core Morello support in the
kernel. The kernel tests are run in the Hybrid-cap ABI only.

The dynamically-linked bionic-unit-tests is only available for the
Hybrid-Cap / Base ABI at the moment, because the Pure-cap ABI is currently
only supported for static executables.

2. servicemanager
-----------------

Source: `servicemanager`_

servicemanager maintains a directory of Android system services and
provides a mapping between the interface name and the Binder handle of the
corresponding service.

In Android/Morello servicemanager is only built for and operates in the Pure-cap
ABI.

While servicemanager has no specific unit tests on its own, the following binder
unit tests are exercising the servicemanager functionality:

- binderLibTest;
- binderThroughputTest;
- binderSafeInterfaceTest.

Note that only the Hybrid-cap ABI is built for these targets, the Pure-cap ABI
is not currently supported for the tests - it is possible for them to exercise
the Pure-cap servicemanager since they are using the Binder-based IPC for that.

Locations in the output file system:

.. code-block:: sh

  /data/nativetest64/binderLibTest/binderLibTest
  /data/nativetest64/binderThroughputTest/binderThroughputTest
  /data/nativetest64/binderSafeInterfaceTest/binderSafeInterfaceTest

3. logd service
---------------

Source: `logd`_

logd is the Android logging daemon.

In Android/Morello logd is only built for and operates in the Pure-cap ABI.

logd can be exercised with the following workloads which are built both for the
Hybrid-cap and Pure-cap ABIs:

- logd-unit-tests
- liblog-unit-tests
- liblog-benchmarks

Locations in the output file system:

.. code-block:: sh

  /data/nativetest64/logd-unit-tests/logd-unit-tests
  /data/nativetest64/liblog-unit-tests/liblog-unit-tests
  /data/benchmarktest64/liblog-benchmarks/liblog-benchmarks

for the Hybrid-cap ABI, and

.. code-block:: sh

  /data/nativetestc64/logd-unit-tests/logd-unit-tests
  /data/nativetestc64/liblog-unit-tests/liblog-unit-tests
  /data/benchmarktestc64/liblog-benchmarks/liblog-benchmarks

for the Pure-cap ABI.

Please, note that some of the unit tests in logd-unit-tests assume certain state
of the logd buffers - specifically on certain logd buffers being non-empty.

4. Basic demo of capability-based compartmentalization
------------------------------------------------------

Source: `Compartment Demo`_

One of the features provided by capability architectures is an alternative way
to implement software compartmentalization. There are many ways to implement
this using Morello and many possible use-cases.

This is a simple demo aimed at demonstrating the principles of
compartmentalization in Morello. It provides a limited framework that allows
multiple compartments to run in the same address space, supported by a
privileged component, the compartment manager.

For more details, please, see: `Compartment Demo Documentation`_

Please, contribute code extending this or providing new demonstrations, sharing
use-cases.

5. Kernel
---------

Source: Kernel_

Like any other architecture extension, Morello requires a number of kernel
changes to enable the use of Morello functionalities in userspace. As part of
this release, a Linux kernel tree with "core" support for Morello is
provided. This core support can be summarized as:

- Runtime feature detection and system initialization.
- Handling of Morello registers (initialization, context-switching, etc.), with
  special care for capability registers that extend standard AArch64 64-bit
  registers.
- Access to capabilities in memory.
- Handling of capability faults.
- Minimal support for the new C64 instruction set.
- Basic ptrace requests for accessing the Morello state.

Fore more information, see: `Kernel Morello documentation`_.

As mentioned previously, this core Morello support does not modify the existing
arm64 kernel-user ABI. This means that backwards-compatibility is preserved, but
also that there is **no support for the Pure-cap ABI**, which is why a userspace
shim is required (see [C.1.1]). Investigations are underway to remove this
requirement by adding support for a new Pure-cap kernel-user ABI.

D. General guidelines for porting to Morello
============================================

The main aspects [F.2] that should be considered when porting software to the
Morello Pure-cap ABI:

- with respect to the memory layout, pointers are 128-bit and must be aligned on
  their size;
- each pointer is in fact a capability i.e. it carries information on the bounds
  of the memory region that can be accessed using the pointer as well as
  permissions on which accesses are allowed;
- there are some restrictions related to arithmetic/bitwise operations on
  pointers.

libarchcap
----------

``libarchcap`` is set of helper macros and functions which aims to provide a
generic way to operate on pointers/capabilities.

Additionally, it works as a form of self-documentation for code changes related
to supporting capabilities.

- ``archcap_c_*`` helpers operate on capabilities and are only available if the
  target supports capabilities;
- the rest of the helpers - ``archcap_*`` - operate on pointers, which are
  capabilities in the Pure-cap ABI and 64-bit integers in the Hybrid-cap / Base
  ABI.

archcap-operations-caps.h_ implements the ``archcap_c_*`` helpers.

archcap-operations-ptrs.h_ implements the ``archcap_*`` helpers.


MORELLO-META mark-up
--------------------

When a particular change is related to Pure-cap ABI support and can't be
expressed in ``archcap`` terms, ``/* MORELLO-META: ... */`` is provided.

For example:

.. code-block:: cpp

  /* MORELLO-META: ptr-not-long */

The complete set of MORELLO-META tags, which are used in this release, is
described in the table below.

+---------------------------+--------------------------------------------------+
| Tag                       | Description                                      |
+===========================+==================================================+
| improvement               | The code has been improved to be more            |
|                           | "Morello-friendly".                              |
|                           |                                                  |
|                           | This is not a modification that is strictly      |
|                           | required.                                        |
+---------------------------+--------------------------------------------------+
| size-not-ptr              | Invalid uintptr_t usage, replaced with size_t.   |
+---------------------------+--------------------------------------------------+
| address-not-ptr           | Invalid uintptr_t usage, replaced with ptraddr_t.|
+---------------------------+--------------------------------------------------+
| ptr-not-long              | Invalid long usage, replaced with uintptr_t.     |
+---------------------------+--------------------------------------------------+
| intcap-upcast             | An integer may be unnecessarily upcast to        |
|                           | (u)intcap_t - for instance if an interface       |
|                           | accepts any kind of integer, while we sometimes  |
|                           | need to preserve capabilities when using it.     |
+---------------------------+--------------------------------------------------+
| capability-fault-expected | In pure mode, a capability fault is expected     |
|                           | here, and the code has been modified to handle   |
|                           | it.                                              |
+---------------------------+--------------------------------------------------+
| padding                   | The code has been modified to handle extra       |
|                           | padding due to capabilities (e.g. in a struct).  |
+---------------------------+--------------------------------------------------+
| stack-size                | Stack size has been increased to cope with the   |
|                           | extra space required by the pure ABI and/or      |
|                           | libshim.                                         |
+---------------------------+--------------------------------------------------+
| pointer-alignment         | The code has been fixed to allow capabilities to |
|                           | be stored in a generic data structure, by        |
|                           | increasing the minimum alignment.                |
|                           |                                                  |
|                           | This is required for instance when               |
|                           | alignof(double) is used, instead of              |
|                           | alignof(uintptr_t). In C(++)11 and later,        |
|                           | alignof(max_align_t) should be used.             |
+---------------------------+--------------------------------------------------+
| use-capability-registers  | The code has been updated to refer to the 129-bit|
|                           | capability registers instead of the generic      |
|                           | 64-bit registers.                                |
+---------------------------+--------------------------------------------------+
| strict-prototypes         | If no parameters list is given in a function     |
|                           | prototype, then any number of arguments of any   |
|                           | types can be passed at a call site using the     |
|                           | prototype. Some of those arguments are to be     |
|                           | passed in registers, while the rest might need   |
|                           | to be passed on stack.                           |
|                           |                                                  |
|                           | In case the prototype doesn't specify parameters |
|                           | and, for example, "0" is passed as an argument,  |
|                           | that might be putting an integer on the stack. On|
|                           | the other hand, passing a capability (pointer) in|
|                           | the same circumstances would result in putting a |
|                           | capability on stack. The sizes of those values   |
|                           | are different, which would result in wrong       |
|                           | offsets for the subsequent arguments put on      |
|                           | stack.                                           |
|                           |                                                  |
|                           | To prevent this kind of errors, the code was     |
|                           | updated to use explicit parameter lists.         |
+---------------------------+--------------------------------------------------+
| capability-bounds         | The code has been modified to make sure that     |
|                           | a capability with the correct bounds is used.    |
+---------------------------+--------------------------------------------------+


Statistics
----------

The statistics are presented as an estimate of changes that had to be introduced
to make the code base compatible with Morello.

+----------------------------+-------------------------------------------------+
| Operation                  | Comments                                        |
+============================+=================================================+
| archcap_address_diff       | 31 uses                                         |
|                            |                                                 |
|                            | Computes difference of addresses within two     |
|                            | given pointers (pointers in Pure-cap are        |
|                            | not just addresses)                             |
+----------------------------+-------------------------------------------------+
| archcap_address_get        | 30 uses                                         |
|                            |                                                 |
|                            | Gets address from pointer                       |
+----------------------------+-------------------------------------------------+
| archcap_address_add        | 24 uses                                         |
|                            |                                                 |
|                            | Adds a value to the address                     |
+----------------------------+-------------------------------------------------+
| archcap_c_from_ddc*        | 21 uses                                         |
|                            |                                                 |
|                            | Reconstructs capability from address and DDC    |
|                            |                                                 |
|                            | Reconstruction of capabilities from DDC is going|
|                            | to be deprecated soon - DDC bounds are to be    |
|                            | reduced, so software components are now advised |
|                            | to keep their own set of root capabilities if   |
|                            | reconstruction from addresses is needed         |
+----------------------------+-------------------------------------------------+
| archcap_address_get_bits   | 17 uses                                         |
|                            |                                                 |
|                            | Gets address with bitmask applied               |
+----------------------------+-------------------------------------------------+
| archcap_nonderef_cast      | 15 uses                                         |
|                            |                                                 |
|                            | Constructs capability that is not supposed to be|
|                            | dereferenced - can be used when an integer value|
|                            | needs to be represented as a pointer, for       |
|                            | example to pass it to a generic interface       |
|                            | which only accepts pointers.                    |
+----------------------------+-------------------------------------------------+
| archcap_address_align_up   | 15 uses                                         |
|                            |                                                 |
|                            | Aligns address up                               |
+----------------------------+-------------------------------------------------+
| archcap_address_align_down | 8 uses                                          |
|                            |                                                 |
|                            | Aligns address down                             |
+----------------------------+-------------------------------------------------+
| archcap_address_sub        | 8 uses                                          |
|                            |                                                 |
|                            | Subtracts a value from the address              |
+----------------------------+-------------------------------------------------+
| archcap_address_clear_bits | 3 uses                                          |
|                            |                                                 |
|                            | Clears bits within a pointer's address according|
|                            | to given mask                                   |
+----------------------------+-------------------------------------------------+
| archcap_address_and        | 3 uses                                          |
|                            |                                                 |
|                            | Performs Bitwise-AND on a pointer's address     |
+----------------------------+-------------------------------------------------+
| archcap_address_set        | 2 uses                                          |
|                            |                                                 |
|                            | Sets pointer address                            |
+----------------------------+-------------------------------------------------+
| archcap_c_ddc_cast         | 1 use                                           |
|                            |                                                 |
|                            | Template-based helper - wrapper over the        |
|                            | archcap_c_from_ddc* helpers                     |
+----------------------------+-------------------------------------------------+
| archcap_address_or         | 1 use                                           |
|                            |                                                 |
|                            | Performs Bitwise-OR on a pointer's address      |
+----------------------------+-------------------------------------------------+

+---------------------------+--------------------------------------------------+
| Tag                       | Comments                                         |
+===========================+==================================================+
| address-not-ptr           | 16 instances                                     |
+---------------------------+--------------------------------------------------+
| ptr-not-long              | 11 instances                                     |
+---------------------------+--------------------------------------------------+
| size-not-ptr              | 7 instances                                      |
+---------------------------+--------------------------------------------------+
| strict-prototypes         | 5 instances                                      |
+---------------------------+--------------------------------------------------+
| intcap-upcast             | 3 instances                                      |
+---------------------------+--------------------------------------------------+
| capability-fault-expected | 2 instances                                      |
+---------------------------+--------------------------------------------------+
| use-capability-registers  | 2 instances                                      |
+---------------------------+--------------------------------------------------+
| capability-bounds         | 1 instance                                       |
+---------------------------+--------------------------------------------------+
| pointer-alignment         | 1 instance                                       |
+---------------------------+--------------------------------------------------+
| padding                   | 1 instance                                       |
+---------------------------+--------------------------------------------------+
| improvement               | 1 instance                                       |
+---------------------------+--------------------------------------------------+
| stack-size                | 1 instance                                       |
+---------------------------+--------------------------------------------------+

E. Ideas on future work
=======================

The initial Morello release is providing minimal support of capabilities and a
few Morello ports. There is plenty of room for further development, research and
reflection on the results.

This section describes potential future directions within the project.

In short-term:

- dynamic linkage support
- restrict boundaries in capabilities returned by memory allocators
- removing use of DDC for reconstructing capabilities after initialization
- narrowing bounds in DDC and PCC to the minimal required range

In middle-term:

- workloads for benchmarking Pure-cap
- demonstrations of Morello implicit benefits for software security
- kernel Pure-cap interface support in Bionic without libshim - once the
  interface is defined
- binder Pure-cap port

Ideas for long-term:

- research of use-cases for and schemes of compartmentalization
- support capabilities in graphics stack once it is enabled
- Pure-cap port of Android Runtime (ART) and Zygote
- many more.

Contributions are very welcome!


F. References
=============

- [1] https://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-941.pdf
- [2] https://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-947.pdf


.. _bionic:
  https://git.morello-project.org/morello/android/platform/bionic/-/tree/morello/release-1.0
.. _libshim:
  https://git.morello-project.org/morello/android/platform/external/libshim/-/blob/morello/release-1.0/README.rst
.. _jemalloc:
  https://git.morello-project.org/morello/android/platform/external/jemalloc_new/-/tree/morello/release-1.0
.. _crtbegin.c:
  https://git.morello-project.org/morello/android/platform/bionic/-/blob/morello/release-1.0/libc/arch-common/bionic/crtbegin.c
.. _morello_init.S:
  https://git.morello-project.org/morello/android/platform/bionic/-/blob/morello/release-1.0/libc/arch-morello/bionic/__morello_init.S
.. _servicemanager:
  https://git.morello-project.org/morello/android/platform/frameworks/native/-/tree/morello/release-1.0/cmds/servicemanager
.. _logd:
  https://git.morello-project.org/morello/android/platform/system/core/-/tree/morello/release-1.0/logd
.. _archcap-operations-caps.h:
  https://git.morello-project.org/morello/android/platform/external/libarchcap/-/blob/morello/release-1.0/include/archcap-operations-caps.h
.. _archcap-operations-ptrs.h:
  https://git.morello-project.org/morello/android/platform/external/libarchcap/-/blob/morello/release-1.0/include/archcap-operations-ptrs.h
.. _Compartment Demo:
  https://git.morello-project.org/morello/android/platform/vendor/arm/morello-examples/-/tree/morello/release-1.0/compartment-demo
.. _Compartment Demo Documentation:
  https://git.morello-project.org/morello/android/platform/vendor/arm/morello-examples/-/blob/morello/release-1.0/compartment-demo/README.rst
.. _Kernel:
  https://git.morello-project.org/morello/kernel/morello-ack/-/tree/morello/release-1.0
.. _Kernel Morello documentation:
  https://git.morello-project.org/morello/kernel/morello-ack/-/blob/morello/release-1.0/Documentation/arm64/morello.rst
