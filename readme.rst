**************************************
Morello Platform Software Repositories
**************************************

This is the top level readme for the Morello platform software repositories.
Please refer to the Open Source Software `landing page`_ for more context.

Morello is a research program and code to support the prototype architecture
will not be upstreamed. Support is maintained as a series of forks within
this hosting. The codebase is intended to provide the necessary platform
support to act as a starting point for collaborative ecosystem development.

Open Source Software hosted in the morello namespace on the hosting
===================================================================

Morello platform Open Source Software (OSS) is delivered as an integrated
software stack. Scripting is provided to build and run the complete stack.

Development is continuous - updated code is pushed to mainline branches.

A release is a set of tagged commits, identifying specific components which
have been integration-tested together.

The `Release Notes`_ provide more information on
specific releases.

Branches structure
------------------

morello/mainline
  Mainline branch name used for repositories which are not derived from an
  external upstream project. The projects without external upstream are
  started for Morello. These branches intrinsically support Morello.

morello/[upstream-branch-name]
  Mainline branch for repositories with an upstream. These branches are based on
  fork points in the corresponding upstream/[upstream-branch-name] branches and host
  the patches adding Morello support. The fork point is specified by the
  morello-[upstream-branch-name]-base tag.

upstream/[upstream-branch-name]
  This branch points to the upstream branch named [upstream-branch-name],
  e.g  `master`_  is taken from the master branch
  of `Trusted Firmware TF-A`_.
  These branches might be updated with upstream changes from time to time
  with the upstream changes being merged to the corresponding Morello branches.

morello/release-Major.Minor
  These forks are created from the morello/mainline or
  morello/[upstream-branch-name] branches for the release.


Overview of deliverables
========================

Firmware
--------

Based on standard open source software components: `SCP firmware`_,
`Trusted Firmware TF-A`_, `UEFI EDK2`_

Integrated Android stack
------------------------

A reduced "nano" Android profile is hosted in the Morello code repositories
and includes:

- Initial modifications to a standard (arm64-v8a) AOSP to support Capabilities
- Basic demo of capability-based compartmentalization
- Modified ACK (Android Common Kernel)
- Prebuilt Android LLVM/Clang toolchain

Please refer to the `Android readme`_ for more details.

Toolchain
---------

Morello is supported by LLVM-based open-source toolchains. These are
experimental toolchains and some features may be missing.

Please refer to the `Morello LLVM Toolchain readme`_ for more details.

Development platforms
=====================

Morello Platform Model
----------------------

The Morello Platform Model is an open access FVP (Fixed Virtual Platform)
implementation of the development platform, aligned with the future development
board. Available to download from Arm's `Ecosystem FVP Developer page`_


Getting started
===============

Please follow the `user guide`_ to sync, build, and run the stack.

Obtaining support
=================

Arm provide a `forum`_  to support general questions on the Morello Platform.

This forum should be used for technical queries related to platform
implementation, usability and architecture specification.

Defects can be raised directly as GitLab issues targeting specific
software component repositories.

Contributions
=============

As the project develops we intend to enable more resources for collaborative
development, including the ability to contribute patches or bug fixes,
supported by a public CI environment.


.. _landing page:
 https://www.morello-project.org

.. _user guide:
 user-guide.rst

.. _Release Notes:
 release-notes.rst

.. _Android readme:
 android-readme.rst

.. _Morello LLVM Toolchain readme:
 toolchain-readme.rst

.. _forum:
 https://community.arm.com/developer/research/morello/f/forum

.. _Ecosystem FVP Developer page:
 https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps

.. _SCP firmware:
 https://github.com/ARM-software/SCP-firmware

.. _Trusted Firmware TF-A:
 https://www.trustedfirmware.org/

.. _master:
 https://git.morello-project.org/morello/trusted-firmware-a/-/tree/upstream/master

.. _UEFI EDK2:
 https://github.com/tianocore/edk2
