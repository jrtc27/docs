*************
Release notes
*************

Release tag
===========

The manifest tag for this release is **morello-release-1.0**

Software Features
=================

The following are a summary of the key software features of this release:

* Android Common Kernel 5.4.50 with Morello Capability Enablement.
  For details please refer to `Kernel Morello documentation`_
* Android Q (10.0.0_r2) nano profile with some components ported to
  Morello Pure-cap ABI. For details please refer to `Android readme`_
* LLVM Toolchain support for Morello Platform.
  For details please refer to `Toolchain readme`_
* Yocto based BSP build supporting Poky Distribution (AArch64).
  Android build scripts.
* Morello Capability aware platform support in Trusted Firmware-A.
* Morello Capability aware platform support in EDK2.
* System Control Processor (SCP) firmware for power & clock control and for
  memory controller & interconnect configuration.

Platform Support
================

* This Software release is tested on Morello Fast Model platform (FVP).

Known issues or Limitations
===========================

* For faster boot, system DRAM is limited to 2GB. For full utilization
  of 8GB DRAM needs a kernel command line edit to remove ``mem=2G``
* The Morello toolchain, which is used to build Android, is based on a much
  more recent version of LLVM that the default prebuilt toolchain in Android 10.
  As a result, a large number of warnings is emitted during the build.
* While booting Android, there are warning or error messages which are
  expected and normal given the reduced profile of the Android - ``avc`` audit
  reports, ``libprocessgroup`` missing profile messages, ``apex`` warnings,
  ``setpgid`` warnings etc.

.. _Kernel Morello documentation:
 https://git.morello-project.org/morello/kernel/morello-ack/-/blob/morello/release-1.0/Documentation/arm64/morello.rst

.. _Android readme:
 android-readme.rst

.. _Toolchain readme:
 toolchain-readme.rst
