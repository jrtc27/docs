***********
User Guide
***********

Notice
======

The Morello software stack consists of capability-aware firmware components
built with Yocto bitbake and Android 10 built with native Android
build system. This environment also provides a choice of AArch64 Poky
Linux distribution, using the same firmware components.

Prerequisites
=============

These instructions assume that:

- Your host PC is running Ubuntu Linux 18.04 LTS and
  should have at least 250GB free storage space.
- You are running the provided scripts in a ``bash`` shell environment.

To ensure that all the required packages are installed, run:

::

    sudo apt-get update
    sudo apt-get install chrpath gawk texinfo libssl-dev diffstat wget \
    git-core unzip gcc-multilib build-essential socat cpio python python3 \
    python3-pip python3-pexpect xz-utils debianutils iputils-ping python3-git \
    python3-jinja2 libegl1-mesa libsdl1.2-dev pylint3 xterm git-lfs openssl \
    curl libncurses-dev libz-dev python-pip repo git-core gnupg flex bison  \
    build-essential zip curl zlib1g-dev gcc-multilib g++-multilib \
    libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev \
    lib32z1-dev libgl1-mesa-dev libxml2-utils xsltproc unzip fontconfig mtools


Syncing and building the source code
====================================

Create a new folder that will be your workspace, which will henceforth be
referred to as ``<morello_workspace>`` in these instructions.

::

    mkdir <morello_workspace>
    cd <morello_workspace>


Board Support Package Stack with Poky distribution
--------------------------------------------------

::

    cd <morello_workspace>/
    repo init -u https://git.morello-project.org/morello/manifest.git -b morello/mainline -g bsp
    repo sync
    cd <morello_workspace>/bsp
    MACHINE=morello-fvp DISTRO=poky . ./conf/setup-environment-morello
    bitbake core-image-minimal

To build a specific major.minor release branch replace ``-b morello/mainline``
with ``-b morello/release-major.minor`` in the ``repo init`` command.

The initial clean build will be lengthy, given that all host utilities are to
be built as well as the target images. This includes host
executables (python, cmake, etc.) and the required toolchain(s).

Once the build is successful, all images will be placed in the
``<morello_workspace>/bsp/build-poky/tmp-poky/deploy/images/morello-fvp``
directory.

Note that the BSP includes the Poky Linux distribution,
which offers BusyBox-like utilities.


Android Stack
-------------

Profiles supported:

morello_nano-fvp : This supports console-only Android profile and provides a
good runtime environment for testing shell-based applications.

For more details please refer to the `Android readme`_

::

    cd <morello_workspace>
    repo init -u https://git.morello-project.org/morello/manifest.git -b morello/mainline -g android
    repo sync
    cd <morello_workspace>/bsp
    MACHINE=morello-fvp DISTRO=poky . ./conf/setup-environment-morello
    bitbake scp-firmware && bitbake edk2-firmware && bitbake grub-efi

    cd <morello_workspace>/android
    # Supported commands are clean, build, package, all (all the three)
    # If no command is specified, defaults to build and package.
    ./build-scripts/build-android.sh <command>

To build a major.minor release branch replace
``-b morello/mainline`` with ``-b morello/release-major.minor``
in the ``repo init`` command.

Provided components
-------------------

Within the Yocto project, each component included in the Morello software stack
is specified as a `Bitbake recipe`_. The morello-fvp recipes are located at
``<morello_workspace>/bsp/layers/meta-arm/``.

Steps for baking Images from unstaged source code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Yocto allows modifying the fetched source code of each recipe component in the
workspace, by applying patches. This is however not a convenient approach for
developers, since creating patches and updating recipes is time-consuming.
To make that easier, Yocto provides the devtool utility. devtool creates a
new workspace, in which you can edit the fetched source code and bake images
with the modifications
::

    cd <morello_workspace>/bsp
    MACHINE=morello-fvp DISTRO=poky . ./conf/setup-environment-morello
    # create a workspace for a given recipe component
    # recipe-component-name can be of:
    # trusted-firmware-a / scp-firmware / edk2-firmware / grub-efi / linux-yocto
    devtool modify <recipe-component-name>
    # This creates a new workspace for recipe-component-name and fetches source code
    # into "build-poky/workspace/sources/{trusted-firmware-a,scp-firmware,edk2-firmware,grub-efi,linux-yocto}"
    # edit the source code in the newly created workspace
    # build images with changes on workspace
    # recipe-component-name can be of: scp-firmware / edk2-firmware / grub-efi / linux-yocto
    bitbake <recipe-component-name>

Software Components
===================

Trusted Firmware-A
------------------

Based on `Trusted Firmware-A`_

+--------+------------------------------------------------------------------------------------------------------------------------+
| Recipe | <morello_workspace>/bsp/layers/meta-arm/meta-arm-bsp/recipes-bsp/trusted-firmware-a/trusted-firmware-a-morello-fvp.inc |
+--------+------------------------------------------------------------------------------------------------------------------------+
| Files  | * <morello_workspace>/bsp/build-poky/tmp-poky/deploy/images/morello-fvp/bl31-morello.bin                               |
+--------+------------------------------------------------------------------------------------------------------------------------+

System Control Processor (SCP)
------------------------------

Based on `SCP Firmware`_

+--------+----------------------------------------------------------------------------------------------------------------+
| Recipe | <morello_workspace>/bsp/layers/meta-arm/meta-arm-bsp/recipes-bsp/scp-firmware/scp-firmware-morello-fvp.inc     |
+--------+----------------------------------------------------------------------------------------------------------------+
| Files  | * <morello_workspace>/bsp/build-poky/tmp-poky/deploy/images/morello-fvp/scp_fw.bin                             |
|        | * <morello_workspace>/bsp/build-poky/tmp-poky/deploy/images/morello-fvp/scp_romfw.bin                          |
|        | * <morello_workspace>/bsp/build-poky/tmp-poky/deploy/images/morello-fvp/mcp_fw.bin                             |
|        | * <morello_workspace>/bsp/build-poky/tmp-poky/deploy/images/morello-fvp/mcp_romfw.bin                          |
+--------+----------------------------------------------------------------------------------------------------------------+

UEFI
----

Based on  `edk2-platforms`_

+--------+-----------------------------------------------------------------------------------------------------+
| Recipe | <morello_workspace>/bsp/layers/meta-arm/meta-arm-bsp/recipes-bsp/uefi/edk2-firmware-morello-fvp.inc |
+--------+-----------------------------------------------------------------------------------------------------+
| Files  | * <morello_workspace>/bsp/build-poky/tmp-poky/deploy/images/morello-fvp/uefi.bin                    |
+--------+-----------------------------------------------------------------------------------------------------+

Grub
----

The layer is based on the `poky`_ grub-efi provider.

+--------+-----------------------------------------------------------------------------------------------+
| Recipe | <morello_workspace>/bsp/meta/recipes-bsp/grub/grub-efi_2.04.bb                                |
+--------+-----------------------------------------------------------------------------------------------+
| Files  | * <morello_workspace>/bsp/build-poky/tmp-poky/deploy/images/morello-fvp/grub-efi-bootaa64.efi |
+--------+-----------------------------------------------------------------------------------------------+

Linux
-----

The layer is based on the `linux kernel`_ yocto mainline provider.

+--------+-------------------------------------------------------------------------------------+
| Recipe | <morello_workspace>/bsp/meta/recipes-kernel/linux/linux-yocto_5.4.bb                |
+--------+-------------------------------------------------------------------------------------+
| Files  | * <morello_workspace>/bsp/build-poky/tmp-poky/deploy/images/morello-fvp/Image       |
+--------+-------------------------------------------------------------------------------------+

Poky distro
-----------

The layer is based on the `poky`_ filesystem distribution.
The provided distribution is based on BusyBox and built using glibc.

+--------+----------------------------------------------------------------------------------------------------------------+
| Recipe | <morello_workspace>/bsp/layers/openembedded-core/meta/recipes-core/images/core-image-minimal.bb                |
+--------+----------------------------------------------------------------------------------------------------------------+
| Files  | * <morello_workspace>bsp/build-poky/tmp-poky/work/morello_fvp-poky-linux/core-image-minimal/1.0-r0/rootfs      |
+--------+----------------------------------------------------------------------------------------------------------------+

Android
-------

Android 10 is supported in this release with device profiles suitable for
Morello machine configuration. Android is built as a separate project and then
booted with the BSP built by Yocto. The images are packaged using scripts
in the ``<morello_workspace>/android/build-scripts`` directory.

Run scripts
-----------

``<morello_workspace>/run-scripts/`` provides scripts
for running the software stack. Usage descriptions for the
various scripts are provided in the following sections.

Running the software on FVP
===========================

The Morello Fixed Virtual Platform (Morello FVP) must be available
to execute the included run scripts.

Please refer to `Arm Ecosystem FVPs`_ to download the Morello FVP.

The run-scripts structure is as follows:

::

    run-scripts
      |--run_model.sh
      |-- ...

Ensure that all dependencies are met by executing the
FVP: ``./path/to/FVP_Morello/models/Linux64_GCC-6.4/FVP_Morello``.
You should see the FVP launch, presenting a graphical interface
showing information about the current state of the FVP.

The ``run_model.sh`` script in ``<morello_workspace>/run-scripts``
will launch the FVP, providing the previously built images as arguments.
Execute the ``run_model.sh`` script:

::

        For Running Poky/Android :
        ./run-scripts/run_model.sh -m <model binary path> -f poky
        OR
        ./run-scripts/run_model.sh -m <model binary path> -f android

When the script is executed, three terminal instances will be launched, one for
the SCP and two for the AP (Application Processor). Once the FVP is running,
the SCP will be the first to boot, bringing the AP out of reset.
The AP will start booting Trusted Firmware-A, then UEFI,
then Linux and Poky/Android.

When booting Poky the model will boot Linux and present a
login prompt. Login using the username ``root``.
You may need to hit Enter for the prompt to appear.

To run terminal-only mode on hosts without graphics/display:

::

        For Running Poky/Android :
        env -u DISPLAY ./run-scripts/run_model.sh -m <model binary path> -f poky
        OR
        env -u DISPLAY ./run-scripts/run_model.sh -m <model binary path> -f android

This launches FVP in the background and automatically connects to the
interactive application console via telnet.

To stop the model, exit telnet:

::

        Ctrl + ]
        telnet> close

To run Android Tests Suites and workloads, please refer to `Workloads`_.

Running the Android Debug Bridge (ADB) with FVP
===============================================

Run Android on FVP. Once the boot sequence has completed
(when the console prompt appears), use the following commands in another shell
to interact with Android via ADB:

::

        cd <morello_workspace>/android/
        source build/envsetup.sh
        lunch morello_fvp_nano-eng
        adb devices
        # If no device appears:
        # 1. FVP tries to bind the internal ADB port to port 5555 on the host. This may
        #    fail if the port is in use, in which case the console will show a message like:
        #    > Info: Morello_Top: board.hostbridge: ...binding to port 5559 instead
        #    If this happens, you will need to connect manually using `adb connect 127.0.0.1:<port>`.
        #    You should also use `adb disconnect 127.0.0.1:<port>` before exiting the model.
        #
        # 2. Even when FVP does bind to port 5555 on the host (no port warning on the console),
        #    the ADB host server might fail to detect it. In that case use the instructions
        #    above with <port>=5555.
        adb shell

.. _Yocto project:
 https://www.yoctoproject.org/

.. _Bitbake:
 https://www.yoctoproject.org/docs/1.6/bitbake-user-manual/bitbake-user-manual.html

.. _Bitbake recipe:
 https://www.yoctoproject.org/docs/1.6/bitbake-user-manual/bitbake-user-manual.html#recipes

.. _Trusted Firmware-A:
 https://trustedfirmware-a.readthedocs.io/en/latest/

.. _SCP Firmware:
 https://github.com/ARM-software/SCP-firmware

.. _edk2-platforms:
 https://git.morello-project.org/morello/edk2-platforms/-/tree/master

.. _poky:
 https://www.yoctoproject.org/software-item/poky/

.. _linux kernel:
 https://gitlab.com/openembedded/community/meta-kernel

.. _Android readme:
 android-readme.rst

.. _Workloads:
 workloads.rst

.. _Arm Ecosystem FVPs:
 https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps
